# NorrisJokeApp

The RWD app displays a list of random chuck norris jokes from `http://api.icndb.com/jokes` also provides the feature to store, delete, update the favorite jokes

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

##Description

  This app follows `domain-style` structure. The app is the root module of the project.
  App contains two modules. and each domain contains a shared module, say example here Dashboard is a domain which will have a shared folder.
  
  - Dashboard
  - Shared
  
  Unit tests are available for all the modules, services and components.
  
### Shared
  The Shared module contains all the common code which is common/shared to the domain.
  There are two shared components
    
  * Alert - a simple alert component to show notifications, errors etc.
  * Spinner - to show loading icon on async events like api calls

### Dashboard
  Dashboard contains the below components
  
   * joke-item - to show the joke item which both joke list and favorite joke list consumes
   * joke-list - to show the list of random jokes
   * fav-joke-list - to show the favourite jokes
   * dashboard - parent component holds joke and fav joke list
   
 Also shared services
 
   * JokeService - contains the http apis to fetch jokes
   * DataService - to share data between different components
   

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
