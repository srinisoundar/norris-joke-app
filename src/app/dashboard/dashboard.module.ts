import {NgModule} from '@angular/core';
import {JokeItemComponent} from './joke-item/joke-item.component';
import {JokeListComponent} from './joke-list/joke-list.component';
import {FavJokeListComponent} from './fav-joke-list/fav-joke-list.component';
import {SharedModule} from './shared/shared.module';
import {SharedModule as AppSharedModule} from '../shared/shared.module';
import {DashboardComponent} from './dashboard.component';


@NgModule({
  declarations: [
    JokeItemComponent,
    JokeListComponent,
    FavJokeListComponent,
    DashboardComponent
  ],
  imports: [
    SharedModule,
    AppSharedModule
  ],
  exports: [
    DashboardComponent
  ]
})
export class DashboardModule { }
