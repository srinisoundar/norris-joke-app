import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FavJokeListComponent} from './fav-joke-list.component';
import {SpinnerComponent} from '../../shared/spinner/spinner.component';
import {AlertComponent} from '../../shared/alert/alert.component';
import {JokeService} from '../shared/joke.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {of} from 'rxjs';
import {DataService} from '../shared/data.service';
import {Joke} from '../shared/joke.model';
import {jokeMock} from '../shared/mock.joke';
import Spy = jasmine.Spy;

describe('FavJokeListComponent', () => {
  let component: FavJokeListComponent;
  let fixture: ComponentFixture<FavJokeListComponent>;
  let getRandomJokesSpy: Spy;
  let dataService: DataService;
  const dummyJokeRes: Joke[] = jokeMock.value;
  const dummyJoke: Joke = {
    id: 1,
    joke: 'dummy joke',
    favourite: true,
    categories: []
  };

  beforeEach(async(() => {
    const jokeService = jasmine.createSpyObj('JokeService', ['getRandomJokes']);
    getRandomJokesSpy = jokeService.getRandomJokes.and.returnValue(of(dummyJokeRes.slice(0, 1)));

    TestBed.configureTestingModule({
      declarations: [FavJokeListComponent, SpinnerComponent, AlertComponent],
      providers: [
        {provide: JokeService, useValue: jokeService}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
    dataService = TestBed.get(DataService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavJokeListComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize the component', () => {
    spyOn(localStorage, 'getItem').and.returnValue(null);
    fixture.detectChanges();
    expect(component.jokes).toEqual([]);
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.card-header h4').textContent).toBe('Favourite Jokes');
  });

  it('should render the jokes from localstorage', () => {
    spyOn(localStorage, 'getItem').and.returnValue(JSON.stringify(dummyJokeRes));
    fixture.detectChanges();
    expect(component.jokes).toEqual(dummyJokeRes);
  });

  it('expect delete to remove the joke and update localstorage', () => {
    spyOn(localStorage, 'setItem');
    fixture.detectChanges();
    component.jokes = dummyJokeRes;
    fixture.detectChanges();
    expect(component.jokes).toEqual(dummyJokeRes);
    dataService.deleteJoke(dummyJokeRes[0], false);
    expect(component.jokes).toEqual(dummyJokeRes.slice(1));
    expect(localStorage.setItem).toHaveBeenCalledWith('jokes', JSON.stringify(component.jokes));
  });

  it('expect favourite joke to be added on true', () => {
    spyOn(localStorage, 'setItem');
    fixture.detectChanges();
    component.jokes = dummyJokeRes;
    fixture.detectChanges();
    expect(component.jokes).toEqual(dummyJokeRes);
    dataService.updateFavourite(dummyJoke);
    expect(component.jokes.length).toEqual(3);
    expect(localStorage.setItem).toHaveBeenCalledWith('jokes', JSON.stringify(component.jokes));
  });

  it('expect favourite joke to be remove on false', () => {
    spyOn(localStorage, 'setItem');
    dummyJokeRes[0].favourite = false;
    fixture.detectChanges();
    component.jokes = dummyJokeRes;
    fixture.detectChanges();
    expect(component.jokes).toEqual(dummyJokeRes);
    dataService.updateFavourite(dummyJokeRes[0]);
    expect(component.jokes.length).toEqual(1);
    expect(localStorage.setItem).toHaveBeenCalledWith('jokes', JSON.stringify(component.jokes));
  });

  it('expect joke not to be added on exceeding `jokeLimit=2`', () => {
    spyOn(localStorage, 'setItem');
    fixture.detectChanges();
    component.jokes = dummyJokeRes;
    component.jokeLimit = 2;
    fixture.detectChanges();
    dataService.updateFavourite(dummyJoke);
    expect(component.jokes.length).toEqual(2);
    expect(localStorage.setItem).not.toHaveBeenCalled();
  });

  it('expect random one joke to be fetched when timer on', () => {
    spyOn(component, 'getTimer').and.returnValue(of(1));
    spyOn(localStorage, 'setItem');
    fixture.detectChanges();
    component.onTimer();
    expect(getRandomJokesSpy).toHaveBeenCalledWith({nos: 1});
    expect(component.loading).toBeFalsy();
    expect(component.jokes).toEqual(dummyJokeRes.slice(0, 1));
    expect(localStorage.setItem).toHaveBeenCalledWith('jokes', JSON.stringify(component.jokes));
  });
});
