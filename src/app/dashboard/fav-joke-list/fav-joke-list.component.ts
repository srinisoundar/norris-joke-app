import {Component, OnInit} from '@angular/core';
import {Joke} from '../shared/joke.model';
import {JokeService} from '../shared/joke.service';
import {DataService} from '../shared/data.service';
import {Observable, Subscription, timer} from 'rxjs';


@Component({
  selector: 'app-fav-joke-list',
  templateUrl: './fav-joke-list.component.html',
  styleUrls: ['./fav-joke-list.component.scss']
})
export class FavJokeListComponent implements OnInit {
  jokes: Joke[];
  loading: boolean;
  error: string;
  timerState: boolean;
  timer: Subscription;
  jokeLimit = 10;

  constructor(private jokeService: JokeService,
              private  dataService: DataService) {
  }

  ngOnInit() {
    this.jokes = this.getStoredJokes();

    // Observable to add/remove favourite joke
    this.dataService.favJoke$.subscribe((joke: Joke) => {
      if (joke.favourite && this.jokes.length < this.jokeLimit) {
        if (this.jokes.findIndex((val: Joke) => val.id === joke.id) < 0) {
          this.jokes = [joke, ...this.jokes];
          this.saveJokes(this.jokes);
        }
      } else if (joke.favourite && this.jokes.length === this.jokeLimit) {
        this.dataService.deleteJoke(joke, true);
      } else {
        this.jokes = this.jokes.filter((curJoke) => curJoke.id !== joke.id);
        this.saveJokes(this.jokes);
      }
    });

    // Observable to delete favourite joke
    this.dataService.delJoke$.subscribe(({joke, softDelete}: { joke?: Joke, softDelete?: boolean }) => {
      if (!softDelete) {
        this.jokes = this.jokes.filter((curJoke) => curJoke.id !== joke.id);
        this.saveJokes(this.jokes);
      }
    });
  }

  /**
   * method to get the jokes in local storage
   */
  private getStoredJokes(): Joke[] {
    const jokes = localStorage.getItem('jokes');
    return jokes ? JSON.parse(jokes) : [];
  }

  /**
   * method to save jokes in local storage
   */
  private saveJokes(jokes: Joke[]): void {
    localStorage.setItem('jokes', JSON.stringify(jokes));
  }

  /**
   * Method to get the one random joke
   */
  private getRandomJoke(): void {
    this.loading = true;
    this.error = '';
    this.jokeService.getRandomJokes({nos: 1})
      .subscribe((jokes: Joke[]) => {
          this.jokes = [...jokes, ...this.jokes];
          this.saveJokes(this.jokes);
        },
        (error) => {
          this.error = error;
          this.timer.unsubscribe();
          this.timerState = false;
          console.log(this.timerState)
        }
      )
      .add(() => this.loading = false);
  }

  /**
   * method triggered on timer click
   */
  onTimer(): void {
    this.timerState = !this.timerState;
    if (this.timerState) {
      // every 5 seconds the timer should add a joke, max of jokeLimit
      this.timer = this.getTimer(5000, 5000)
        .subscribe(() => {
          if (this.jokes.length === this.jokeLimit) {
            this.timer.unsubscribe();
            this.timerState = false;
          } else {
            this.getRandomJoke();
          }
        });
    } else {
      this.timer.unsubscribe();
    }
  }

  getTimer(dueTime: number, period: number): Observable<number> {
    return timer(dueTime, period);
  }

  /**
   * Method to track the joke by id
   * @return joke id
   */
  trackByJokes(index: number, joke: Joke): number {
    return joke.id;
  }
}
