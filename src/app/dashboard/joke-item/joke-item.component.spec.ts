import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JokeItemComponent } from './joke-item.component';
import {DataService} from '../shared/data.service';
import {Joke} from '../shared/joke.model';
import {By} from '@angular/platform-browser';

describe('JokeItemComponent', () => {
  let component: JokeItemComponent;
  let fixture: ComponentFixture<JokeItemComponent>;
  let dataService: DataService;
  const dummyJoke: Joke = {
    id: 1,
    joke: 'dummy joke',
    favourite: false,
    categories: []
  };

  beforeEach(async(() => {
    dataService = jasmine.createSpyObj('DataService', ['updateFavourite', 'deleteJoke']);

    TestBed.configureTestingModule({
      declarations: [ JokeItemComponent ],
      providers: [{ provide: DataService, useValue: dataService }],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JokeItemComponent);
    component = fixture.componentInstance;
  });

  it('should create joke-item component', () => {
    expect(component).toBeTruthy();
  });

  it('should display joke', () => {
    component.joke = dummyJoke;
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.joke-item__val').innerHTML).toBe(dummyJoke.joke);
    expect(compiled.querySelector('.joke-item-heart__icon')).toBe(null);
    expect(compiled.querySelector('.joke-item-delete__icon')).toBe(null);
  });

  it('should display favourite icon ', () => {
    component.joke = dummyJoke;
    component.showFavIcon = true;
    component.showDelIcon = false;
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.joke-item__val').innerHTML).toBe(dummyJoke.joke);
    expect(compiled.querySelector('.joke-item-heart__icon')).toBeDefined();
    expect(compiled.querySelector('.joke-item-delete__icon')).toBe(null);
  });

  it('should display delete icon ', () => {
    component.joke = dummyJoke;
    component.showFavIcon = false;
    component.showDelIcon = true;
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.joke-item__val').innerHTML).toBe(dummyJoke.joke);
    expect(compiled.querySelector('.joke-item-heart__icon')).toBe(null);
    expect(compiled.querySelector('.joke-item-delete__icon')).toBeDefined();
  });

  it('expect `updateFavourite` method to be triggered on fav icon click', () => {
    component.joke = dummyJoke;
    component.showFavIcon = true;
    fixture.detectChanges();
    const btn = fixture.debugElement.query(By.css('.joke-item-heart__icon'));
    btn.triggerEventHandler('click', dummyJoke);
    dummyJoke.favourite = !dummyJoke.favourite;
    expect(dataService.updateFavourite).toHaveBeenCalledWith(dummyJoke);
  });

  it('expect `deleteJoke` method to be triggered on delete icon click', () => {
    component.joke = dummyJoke;
    component.showDelIcon = true;
    fixture.detectChanges();
    const btn = fixture.debugElement.query(By.css('.joke-item-delete__icon'));
    btn.triggerEventHandler('click', dummyJoke);
    expect(dataService.deleteJoke).toHaveBeenCalledWith(dummyJoke, false);
  });
});
