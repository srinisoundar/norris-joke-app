import {Component, Input, OnInit} from '@angular/core';
import {Joke} from '../shared/joke.model';
import {DataService} from '../shared/data.service';

@Component({
  selector: 'app-joke-item',
  templateUrl: './joke-item.component.html',
  styleUrls: ['./joke-item.component.scss']
})
export class JokeItemComponent implements OnInit {
  @Input() showFavIcon: boolean;
  @Input() showDelIcon: boolean;
  @Input() joke: Joke;
  isOver: boolean;

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
  }

  onFavoriteSelect(joke: Joke): void {
    joke.favourite = !joke.favourite;
    this.dataService.updateFavourite(joke);
  }

  onDelete(joke: Joke): void {
    this.dataService.deleteJoke(joke, false);
  }

}
