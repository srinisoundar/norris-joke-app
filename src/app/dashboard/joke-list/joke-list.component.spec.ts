import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {JokeListComponent} from './joke-list.component';
import {DataService} from '../shared/data.service';
import {Joke} from '../shared/joke.model';
import {jokeMock} from '../shared/mock.joke';
import {JokeService} from '../shared/joke.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {SpinnerComponent} from '../../shared/spinner/spinner.component';
import {AlertComponent} from '../../shared/alert/alert.component';
import {of, throwError} from 'rxjs';
import {By} from '@angular/platform-browser';
import Spy = jasmine.Spy;

describe('JokeListComponent', () => {
  let component: JokeListComponent;
  let fixture: ComponentFixture<JokeListComponent>;
  let getRandomJokesSpy: Spy;
  let dataService: DataService;
  const dummyJokeRes: Joke[] = jokeMock.value;

  beforeEach(async(() => {
    const jokeService = jasmine.createSpyObj('JokeService', ['getRandomJokes']);
    getRandomJokesSpy = jokeService.getRandomJokes.and.returnValue(of(dummyJokeRes));

    TestBed.configureTestingModule({
      declarations: [ JokeListComponent, SpinnerComponent, AlertComponent ],
      providers: [
          { provide: JokeService, useValue: jokeService }
        ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
    dataService = TestBed.get(DataService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JokeListComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize the component', () => {
    fixture.detectChanges();
    expect(getRandomJokesSpy).toHaveBeenCalledWith({nos: 10});
    expect(component.loading).toBeFalsy();
    expect(component.jokes).toBe(dummyJokeRes);
  });

  it('should refresh jokes on refresh click', () => {
    fixture.detectChanges();
    expect(getRandomJokesSpy).toHaveBeenCalledWith({nos: 10});
    expect(component.loading).toBeFalsy();
    expect(component.jokes).toBe(dummyJokeRes);
    const btn = fixture.debugElement.query(By.css('.card-header  .btn'));
    btn.triggerEventHandler('click', null);
    expect(getRandomJokesSpy).toHaveBeenCalledWith({nos: 10});
    expect(component.loading).toBeFalsy();
    expect(component.jokes).toBe(dummyJokeRes);
  });

  it('expect the joke to be marked as not favourite', () => {
    dummyJokeRes[0].favourite = true;
    fixture.detectChanges();
    expect(component.jokes[0].favourite).toBe(true);
    dataService.deleteJoke(dummyJokeRes[0], false);
    expect(component.jokes[0].favourite).toBe(false);
  });

  it('should render the jokes', () => {
    spyOn(component, 'trackByJokes');
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.card-header h4').textContent).toBe('Jokes');
    expect(component.trackByJokes).toHaveBeenCalled();
  });

  it('should show error on `getRandomJokes` throws error', () => {
    getRandomJokesSpy.and.returnValue(throwError('network error'));
    fixture.detectChanges();
    expect(component.error).toBe('network error');
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.alert').textContent).toContain('network error');
  });

});
