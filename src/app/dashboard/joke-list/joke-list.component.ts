import { Component, OnInit } from '@angular/core';
import {JokeService} from '../shared/joke.service';
import {Joke} from '../shared/joke.model';
import {DataService} from '../shared/data.service';

@Component({
  selector: 'app-joke-list',
  templateUrl: './joke-list.component.html',
  styleUrls: ['./joke-list.component.scss']
})
export class JokeListComponent implements OnInit {
  jokes: Joke[];
  loading: boolean;
  error: string;
  jokeLimit = 10;

  constructor(private jokeService: JokeService,
              private  dataService: DataService) {
  }

  ngOnInit() {
    this.jokes = [];
    // Observable to mark it as a not favourite joke
    this.dataService.delJoke$.subscribe(({joke}: {joke?: Joke}) => {
      const index = this.jokes.findIndex((curJoke) => curJoke.id === joke.id);
      if (index >= 0) {
        this.jokes[index].favourite = false;
      }
    });
    this.getRandomJokes();
  }

  /**
   * Method to get the random 10 jokes
   */
  private getRandomJokes(): void {
    this.loading = true;
    this.error = '';
    this.jokeService.getRandomJokes({nos: this.jokeLimit})
      .subscribe((jokes: Joke[]) => {
          this.jokes = jokes;
      },
        (error) => this.error = error
      )
      .add(() => this.loading = false);
  }

  /**
   * method triggered on refresh click
   */
  onRefresh(): void {
    this.getRandomJokes();
  }

  /**
   * Method to track the joke by id
   * @return joke id
   */
  trackByJokes(index: number, joke: Joke): number {
    return joke.id;
  }

}
