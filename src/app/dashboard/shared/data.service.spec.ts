import { TestBed } from '@angular/core/testing';

import { DataService } from './data.service';
import {Joke} from './joke.model';

describe('DataService', () => {
  let service: DataService;
  const dummyJoke: Joke = {
    id: 1,
    joke: 'dummy joke',
    favourite: false,
    categories: []
  };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(DataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Method: `updateFavourite` should emit correct data', () => {
    service.favJoke$.subscribe((joke) => {
      expect(joke).toEqual(dummyJoke);
    });
    service.updateFavourite(dummyJoke);
  });

  it('Method: `deleteJoke` should emit correct data', () => {
    service.delJoke$.subscribe(({joke, softDelete}) => {
      expect(joke).toEqual(dummyJoke);
      expect(softDelete).toBeTruthy();
    });
    service.deleteJoke(dummyJoke, true);
  });
});
