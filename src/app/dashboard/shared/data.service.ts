import { Injectable } from '@angular/core';
import { Subject} from 'rxjs';
import {Joke} from './joke.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private favJokeSource = new Subject();
  private delJokeSource = new Subject();

  favJoke$ = this.favJokeSource.asObservable();
  delJoke$ = this.delJokeSource.asObservable();

  constructor() { }

  updateFavourite(joke: Joke): void {
    this.favJokeSource.next(joke);
  }

  deleteJoke(joke: Joke, softDelete: boolean): void {
    this.delJokeSource.next({joke, softDelete});
  }
}
