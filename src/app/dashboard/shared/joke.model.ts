export interface Joke {
  id: number;
  joke: string;
  favourite: boolean;
  categories: string[];
}

export interface JokeResponse {
  type: string;
  value: Joke[];
}
