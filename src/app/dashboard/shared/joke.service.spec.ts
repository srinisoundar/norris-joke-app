import {TestBed} from '@angular/core/testing';

import {JokeService} from './joke.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {environment} from '../../../environments/environment';
import {jokeMock} from './mock.joke';
import {Joke} from './joke.model';

describe('JokeService', () => {
  let service: JokeService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });

    service = TestBed.get(JokeService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getRandomJokes', () => {
    const nos = 2;
    const randomJokeUrl = `${environment.baseUrl}/jokes/random/${nos}`;

    it('should return an Observable<Joke[]>', () => {
      const dummyJokes = jokeMock;

      service.getRandomJokes({nos}).subscribe((jokes: Joke[]) => {
        expect(jokes.length).toBe(2);
        expect(jokes).toBe(jokeMock.value);
      });

      const req = httpTestingController.expectOne(randomJokeUrl);
      expect(req.request.method).toBe('GET');
      req.flush(dummyJokes);
    });

    it('expect jokes to be empty when value not defined', () => {
      service.getRandomJokes({nos}).subscribe((jokes: Joke[]) => {
        expect(jokes.length).toBe(0);
      });
      const req = httpTestingController.expectOne(randomJokeUrl);
      req.flush({});
    });

    it('should return an technical error when the server returns a 500', () => {
      service.getRandomJokes({nos}).subscribe(
        () => fail('should have failed with the 500 error'),
        (error) => {
          expect(error).toBe('Technical error; please try again later.');
        });

      const req = httpTestingController.expectOne(randomJokeUrl);
      req.flush('500 error', {status: 500, statusText: 'Internal server error'});
    });

    it('should return an network error when the server returns a 404', () => {
      service.getRandomJokes({nos}).subscribe(
        () => fail('should have failed with the 404 error'),
        (error) => {
          expect(error).toBe('Network error; please try again later.');
        });

      const req = httpTestingController.expectOne(randomJokeUrl);
      const e = new ErrorEvent('network error');
      req.flush(e, {status: 404, statusText: 'Not Found'});
    });
  });
});
