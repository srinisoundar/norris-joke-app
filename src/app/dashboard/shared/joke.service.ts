import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';

import {catchError, map} from 'rxjs/operators';
import {Joke, JokeResponse} from './joke.model';

@Injectable({
  providedIn: 'root'
})
export class JokeService {
  private randomJokeUrl = `${environment.baseUrl}/jokes/random`;

  constructor(private http: HttpClient) {}

  /**
   * Method to fetch the random norris jokes
   * @param options - contains the number of jokes to fetch
   * @return list of jokes
   */
  getRandomJokes({nos = 1 }: {nos?: number} = {}): Observable<Joke[]> {
    return this.http.get<JokeResponse>(`${this.randomJokeUrl}/${nos}`)
      .pipe(
        map(({value = []}: {value?: Joke[]} = {}) => {
          for (const joke of value) {
            joke.favourite = false;
          }
          return value;
        }),
        catchError(this.handleError)
      );
  }

  /**
   * Method to handle error for each request
   */
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
      return throwError('Network error; please try again later.');
    } else {
      console.error(`Backend returned code ${error.status}, body was: ${error.error.message}`);
    }
    // return an observable with a user-facing error message
    return throwError('Technical error; please try again later.');
  }
}
