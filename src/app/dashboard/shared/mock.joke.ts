export const jokeMock = {
  value: [
    {
      id: 100,
      joke: "Chuck Norris grinds his coffee with his teeth and boils the water with his own rage.",
      categories: [],
      favourite: false
    },
    {
      id: 383,
      joke: "The Drummer for Def Leppard's only got one arm. Chuck Norris needed a back scratcher.",
      categories: [],
      favourite: false
    }
  ]
};
